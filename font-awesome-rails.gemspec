#########################################################
# This file has been automatically generated by gem2tgz #
#########################################################
# -*- encoding: utf-8 -*-
# stub: font-awesome-rails 4.7.0.7 ruby lib

Gem::Specification.new do |s|
  s.name = "font-awesome-rails".freeze
  s.version = "4.7.0.7"

  s.required_rubygems_version = Gem::Requirement.new(">= 0".freeze) if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib".freeze]
  s.authors = ["bokmann".freeze]
  s.date = "2021-02-10"
  s.description = "font-awesome-rails provides the Font-Awesome web fonts and stylesheets as a Rails engine for use with the asset pipeline.".freeze
  s.email = ["dbock@codesherpas.com".freeze]
  s.files = ["LICENSE".freeze, "README.md".freeze, "Rakefile".freeze, "app/assets/fonts/FontAwesome.otf".freeze, "app/assets/fonts/fontawesome-webfont.eot".freeze, "app/assets/fonts/fontawesome-webfont.svg".freeze, "app/assets/fonts/fontawesome-webfont.ttf".freeze, "app/assets/fonts/fontawesome-webfont.woff".freeze, "app/assets/fonts/fontawesome-webfont.woff2".freeze, "app/assets/stylesheets/font-awesome.css.erb".freeze, "app/helpers/font_awesome/rails/icon_helper.rb".freeze, "lib/font-awesome-rails.rb".freeze, "lib/font-awesome-rails/engine.rb".freeze, "lib/font-awesome-rails/version.rb".freeze, "test/dummy/.gitignore".freeze, "test/dummy/app/assets/config/manifest.js".freeze, "test/dummy/app/assets/stylesheets/sass-import.css.sass".freeze, "test/dummy/app/assets/stylesheets/scss-import.css.scss".freeze, "test/dummy/app/assets/stylesheets/sprockets-require.css".freeze, "test/dummy/app/controllers/pages_controller.rb".freeze, "test/dummy/app/views/pages/icons.html.erb".freeze, "test/dummy/config.ru".freeze, "test/dummy/config/application.rb".freeze, "test/dummy/config/boot.rb".freeze, "test/dummy/config/environment.rb".freeze, "test/dummy/config/initializers/secret_token.rb".freeze, "test/dummy/config/routes.rb".freeze, "test/font_awesome_rails_test.rb".freeze, "test/icon_helper_test.rb".freeze, "test/test_helper.rb".freeze]
  s.homepage = "https://github.com/bokmann/font-awesome-rails".freeze
  s.licenses = ["MIT".freeze, "SIL Open Font License".freeze]
  s.required_ruby_version = Gem::Requirement.new(">= 1.9.3".freeze)
  s.rubygems_version = "2.7.6.2".freeze
  s.summary = "an asset gemification of the font-awesome icon font library".freeze
  s.test_files = ["test/dummy/.gitignore".freeze, "test/dummy/app/assets/config/manifest.js".freeze, "test/dummy/app/assets/stylesheets/sass-import.css.sass".freeze, "test/dummy/app/assets/stylesheets/scss-import.css.scss".freeze, "test/dummy/app/assets/stylesheets/sprockets-require.css".freeze, "test/dummy/app/controllers/pages_controller.rb".freeze, "test/dummy/app/views/pages/icons.html.erb".freeze, "test/dummy/config.ru".freeze, "test/dummy/config/application.rb".freeze, "test/dummy/config/boot.rb".freeze, "test/dummy/config/environment.rb".freeze, "test/dummy/config/initializers/secret_token.rb".freeze, "test/dummy/config/routes.rb".freeze, "test/font_awesome_rails_test.rb".freeze, "test/icon_helper_test.rb".freeze, "test/test_helper.rb".freeze]

  if s.respond_to? :specification_version then
    s.specification_version = 4

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_development_dependency(%q<activesupport>.freeze, [">= 0"])
      s.add_runtime_dependency(%q<railties>.freeze, ["< 7", ">= 3.2"])
      s.add_development_dependency(%q<sassc-rails>.freeze, [">= 0"])
    else
      s.add_dependency(%q<activesupport>.freeze, [">= 0"])
      s.add_dependency(%q<railties>.freeze, ["< 7", ">= 3.2"])
      s.add_dependency(%q<sassc-rails>.freeze, [">= 0"])
    end
  else
    s.add_dependency(%q<activesupport>.freeze, [">= 0"])
    s.add_dependency(%q<railties>.freeze, ["< 7", ">= 3.2"])
    s.add_dependency(%q<sassc-rails>.freeze, [">= 0"])
  end
end
